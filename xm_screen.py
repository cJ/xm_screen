#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Manage screen sessions


import sys, io, os, re, subprocess, logging
import types

logger = logging.getLogger()


def yaml_escape(x):
	if x is None:
		return "null"
	else:
		return "\"{}\"".format(x)


def screen(args):
	cmd = ["screen"] + args
	return subprocess.call(cmd)


def screen_cmd(args):
	return screen(["-X"] + args)


def screen_query(arg):
	return subprocess.check_output(["screen", "-Q", arg]).decode("utf-8")


def win_with_cmd(num, name, cmd):
	screen(["-t", name, "{}".format(num)])
	screen_cmd(["select", "{}".format(num)])
	screen_cmd(cmd)


def win_with_args(num, name, args):
	screen(["-t", name, "{}".format(num)] + args)
	screen_cmd(["select", "{}".format(num)])


def current():
	return screen_query("number").split(" ")[0]


def get_id_flags_and_name(x):
	m = re.match("^(?P<id>\d+)(?P<flags>\S+)? (?P<name>.*)$", x)
	id_ = int(m.group("id"))
	flags = m.group("flags")
	name = m.group("name")
	return id_, flags, name

def list_windows_adv():
	res = screen_query("windows")
	windows_strs = res.split("  ")
	wins = [ get_id_flags_and_name(x) for x in windows_strs ]
	return wins


def list_windows():
	return [(id_, name) for (id_, flags, name) in list_windows_adv() ]


def close_window(arg):
	if isinstance(arg, int):
		screen_cmd(["select", "{}".format(arg)])
		screen_cmd(["kill"])
	if isinstance(arg, str):
		wins = list_windows()
		for _id, name in wins:
			m = re.match(arg, name)
			if m is not None:
				close_window(_id)


def get_cfg():
	# Create new module
	cfg = types.ModuleType("xm_screen_cfg")
	cfg.__dict__.update(globals())
	cfgfile = os.path.expanduser("~/.config/xm_screen")
	if os.path.exists(cfgfile):
		try:
			with io.open(cfgfile, "rb") as f:
				# Parse script
				code = compile(f.read(), cfgfile, "exec")
				# Populate module
				exec(code, cfg.__dict__)
		except Exception as e:
			logger.warning("Couldn't read config %s: %s", cfgfile, e, exc_info=True)
	cfgdir = os.path.expanduser("~/.config/xm_screen.d")
	if os.path.isdir(cfgdir):
		for f in sorted(os.listdir(cfgdir)):
			if f.endswith("~"):
				continue
			if f.startswith("."):
				continue
			cfgfile = os.path.join(cfgdir, f)
			try:
				with io.open(cfgfile, "rb") as f:
					# Parse script
					code = compile(f.read(), cfgfile, "exec")
					# Populate module
					exec(code, cfg.__dict__)
			except Exception as e:
				logger.warning("Couldn't read config %s: %s", cfgfile, e, exc_info=True)
	return cfg


def main():
	import argparse

	parser = argparse.ArgumentParser(
	 description="Screen session manager",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)

	def do_openclose(args):
		cfg = get_cfg()
		fn = "group_{}_{}".format(args.group, args.command)
		getattr(cfg, fn)()

	subp = subparsers.add_parser(
	 "open",
	 help="Open a group of windows",
	)
	subp.set_defaults(func=do_openclose)

	subp.add_argument("group",
	)

	subp = subparsers.add_parser(
	 "close",
	 help="Close a group of windows",
	)
	subp.set_defaults(func=do_openclose)

	subp.add_argument("group",
	)

	def do_listwindows(args):
		print("windows:")
		for id_, flags, name in list_windows_adv():
			print("- id: {}".format(id_))
			print("  flags: {}".format(yaml_escape(flags)))
			print("  name: {}".format(yaml_escape(name)))

	subp = subparsers.add_parser(
	 "list",
	 help="List session windows (in YAML format)",
	)
	subp.set_defaults(func=do_listwindows)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)


	if getattr(args, 'func', None) is None:
		parser.print_help()
		return 1
	else:
		return args.func(args)


if __name__ == "__main__":
	ret = main()
	raise SystemExit(ret)


