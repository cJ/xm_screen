.. -*- coding: utf-8; indent-tabs-mode:nil; -*-

#########
xm_screen
#########

:License: MIT
:Author: cJ


xm_screen allows to store screen sessions in config files with a
not-too-shitty syntax.

Usage
#####


Opening a group:

.. code:: sh

   xm_screen open example

Closing a group:

.. code:: sh

   xm_screen close example




Configuration
#############

See example file `example/config`.

xm_screen reads configuration files (which are Python scripts!),
as the `~/.config/xm_screen` or files in `~/.config/xm_screen.d/`.

